# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, backref
from workflow.validation.config import Base
from workflow.validation import interfaces

from zope.interface import implements

class Workflowv(Base):
    implements(interfaces.IWorkflowv)
    __tablename__ = 'workflowv'

    id = Column(Integer, primary_key = True)
    titulo = Column(String)

    def __init__(self, titulo):
        self.titulo = titulo


class Estado(Base):
    implements(interfaces.IEstado)
    __tablename__ = 'estado'

    id = Column(Integer, primary_key = True)
    workflowv_id = Column(Integer, ForeignKey('workflowv.id'), nullable=False)
    workflowv = relationship("Workflowv", backref="estado")
    titulo = Column(String)

    def __init__(self, workflowv_id, titulo):
        self.workflowv_id = workflowv_id
        self.titulo = titulo


class Papel(Base):
    implements(interfaces.IPapel)
    __tablename__ = 'papel'

    id = Column(Integer, primary_key = True)
    titulo = Column(String)

    def __init__(self, titulo):
        self.titulo = titulo


class Permissao(Base):
    implements(interfaces.IPermissao)
    __tablename__ = 'permissao'

    id = Column(Integer, primary_key = True)
    titulo = Column(String)
    titulo_permissao = Column(String)

    def __init__(self, titulo, titulo_permissao):
        self.titulo = titulo
        self.titulo_permissao = titulo_permissao


class Ligacao(Base):
    __tablename__ = 'ligacao'

    estado_id = Column(Integer, ForeignKey('estado.id', onupdate="CASCADE", ondelete="CASCADE"), primary_key=True)
    estado = relationship("Estado", backref='ligacao')
    papel_id = Column(Integer, ForeignKey('papel.id', onupdate="CASCADE", ondelete="CASCADE"), primary_key=True)
    papel = relationship("Papel", backref='ligacao')
    permissao_id = Column(Integer, ForeignKey('permissao.id', onupdate="CASCADE", ondelete="CASCADE"), primary_key=True)
    permissao = relationship("Permissao", backref='ligacao')

    def __init__(self, estado_id, papel_id, permissao_id):
        self.estado_id = int(estado_id)
        self.papel_id = int(papel_id)
        self.permissao_id = int(permissao_id)


def add_contents_db(value):
    """
        Add content in DB
        bin/zopepy 
        from workflow.validation.db import add_contents_db
        add_contents_db(True)
    """
    if not value:
        return False
    from sqlalchemy import create_engine
    from sqlalchemy.orm import create_session
    engine = create_engine('postgresql+psycopg2://postgres:postgres@localhost/workflow')
    session = create_session(bind=engine)

    w1 = Workflowv("simple_publication_workflow")
    session.add(w1)
    session.flush()

    e1 = Estado(w1.id,"pending")
    e2 = Estado(w1.id,"private")
    e3 = Estado(w1.id,"published")
    session.add_all([e1,e2,e3])
    session.flush()

    p1 = Papel("Anonymous")
    p2 = Papel("Authenticated")
    p3 = Papel("Contributor")
    p4 = Papel("Editor")
    p5 = Papel("Manager")
    p6 = Papel("Member")
    p7 = Papel("Owner")
    p8 = Papel("Reader")
    p9 = Papel("Reviewer")
    p10 = Papel("Site Administrator")
    session.add_all([p1,p2,p3,p4,p5,p6,p7,p8,p9,p10])
    session.flush()

    per1 = Permissao("Acessar o conteudo","Access contents information")
    per2 = Permissao("Modificar Eventos","Change portal events")
    per3 = Permissao("Modificar conteudo do portal","Modify portal content")
    per4 = Permissao("Visualizar","View")
    session.add_all([per1,per2,per3,per4])
    session.flush()

    l1 = Ligacao(e1.id, p3.id, per1.id)
    l2 = Ligacao(e1.id, p4.id, per1.id)
    l3 = Ligacao(e1.id, p5.id, per1.id)
    l4 = Ligacao(e1.id, p7.id, per1.id)
    l5 = Ligacao(e1.id, p8.id, per1.id)
    l6 = Ligacao(e1.id, p9.id, per1.id)
    l7 = Ligacao(e1.id, p10.id, per1.id)
    l8 = Ligacao(e1.id, p5.id, per2.id)
    l9 = Ligacao(e1.id, p9.id, per2.id)
    l10 = Ligacao(e1.id, p10.id, per2.id)
    l11 = Ligacao(e1.id, p5.id, per3.id)
    l12 = Ligacao(e1.id, p9.id, per3.id)
    l13 = Ligacao(e1.id, p10.id, per3.id)
    l14 = Ligacao(e1.id, p3.id, per4.id)
    l15 = Ligacao(e1.id, p4.id, per4.id)
    l16 = Ligacao(e1.id, p5.id, per4.id)
    l17 = Ligacao(e1.id, p7.id, per4.id)
    l18 = Ligacao(e1.id, p8.id, per4.id)
    l19 = Ligacao(e1.id, p9.id, per4.id)
    l20 = Ligacao(e1.id, p10.id, per4.id)
    session.add_all([l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14,l15,l16,l17,l18,l19,l20])
    session.flush()
