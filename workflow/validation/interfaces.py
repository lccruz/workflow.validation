# -*- coding: utf-8 -*-

from plone.directives import form
from zope import interface, schema
from workflow.validation.config import MessageFactory as _

# Formulários #

class IWorkflowv(form.Schema):
    """ 
        Interface que descreve representanção de Workflow.
    """

    form.mode(id='hidden')
    id = schema.Int(
        title=_(u'ID'),
        description=_(u'Identificador da UF.'),
        required=False)

    titulo = schema.TextLine(
        title=_(u'Título'),
        description=_(u'Título do workflow'))


class IEstado(form.Schema):
    """ 
        Interface que descreve representanção do Estado.
    """

    form.mode(id='hidden')
    id = schema.Int(
        title=_(u'ID'),
        description=_(u'Identificador do Estado.'),
        required=False)

    workflowv_id = schema.Choice(
        title=_(u'Workflow'),
        description=_(u'Selecione o Workflow.'),
        required=True,
        vocabulary='workflow.validation.wv-vocab')

    titulo = schema.TextLine(
        title=_(u'Título'),
        description=_(u'Título do Estado'))



class IPapel(form.Schema):
    """ 
        Interface que descreve representanção de Papel.
    """

    form.mode(id='hidden')
    id = schema.Int(
        title=_(u'ID'),
        description=_(u'Identificador do Papel.'),
        required=False)

    titulo = schema.TextLine(
        title=_(u'Título'),
        description=_(u'Título do papel'))


class IPermissao(form.Schema):
    """ 
        Interface que descreve representanção de Permissao.
    """

    form.mode(id='hidden')
    id = schema.Int(
        title=_(u'ID'),
        description=_(u'Identificador do Permissao.'),
        required=False)

    titulo = schema.TextLine(
        title=_(u'Título'),
        description=_(u'Título da permissao'))

    titulo_permissao = schema.Choice(
        title=_(u'Permissão'),
        description=_(u'Selecione a Permissão.'),
        required=True,
        vocabulary='workflow.validation.permission-vocab')


class ILigacao(form.Schema):
    """ 
        Interface que descreve representanção das Ligações.
    """
