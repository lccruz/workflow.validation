# -*- coding: utf-8 -*-
from z3c.saconfig import named_scoped_session
from workflow.validation.config import Base
from workflow.validation.config import SCOPED_SESSION_NAME

Session = named_scoped_session(SCOPED_SESSION_NAME)

def isNotCoPRelational(context):
    return context.readDataFile("workflow.validation.marker") is None

def createTables(context):
    """Creates tables
    """
    if isNotCoPRelational(context):
        return

    Base.metadata.create_all(bind=Session.bind)
