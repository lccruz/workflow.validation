# -*- coding: utf-8 -*-

from sqlalchemy.exc import IntegrityError

from five import grok
from z3c.form import button
from plone.directives import form
from plone.app.layout.navigation.interfaces import INavigationRoot
from Products.statusmessages.interfaces import IStatusMessage

from workflow.validation.config import MessageFactory as _
from workflow.validation.config import session
from workflow.validation.db import *
from workflow.validation.nav import go
from workflow.validation.interfaces import ILigacao

class LigacaoAddForm(form.SchemaForm):
    """
        Formulário de Ligacao
    """

    grok.context(INavigationRoot)
    grok.name('add-ligacao')
    grok.require('cmf.ManagePortal')

    ignoreContext = True

    schema = ILigacao
    
    def update(self):
        status = IStatusMessage(self.request)
        self.request.set('disable_border', True)
        self.papeis = []
        self.permissoes = []
        self.ligacoes = []
        estado_id = session.query(Estado).get(self.request.get("id"))
        if estado_id:
            self.estado = estado_id.id
        else:
            self.estado = ''
            msg = _(u'Id inválido')
            status.add(msg,'erro')
            return
        items = session.query(Papel).order_by(Papel.titulo).all()
        for item in items:
            self.papeis.append({'id': item.id,
                                'titulo': item.titulo})

        items = session.query(Permissao).order_by(Permissao.titulo).all()
        for item in items:
            self.permissoes.append({'id': item.id,
                                    'titulo': item.titulo})
        #edit
        ligacoes = session.query(Ligacao).filter_by(estado_id=self.estado)
        for ligacao in ligacoes:
            self.ligacoes.append('%s|%s|%s' % (self.estado,ligacao.permissao_id,ligacao.papel_id))
        super(LigacaoAddForm, self).update()

    @button.buttonAndHandler(_(u'Salvar'), name='salvar')
    def handleSalvar(self, action):
        form = self.request.form
        status = IStatusMessage(self.request)
        if len(form.keys()) == 1:
            msg = _(u'Preencher ao menos uma opcao')
            status.add(msg,'erro')
            return
        estado = session.query(Estado).get(form.pop('form.widgets.id'))
        if estado:
            estado_id = estado.id
        else:
            msg = _(u'Id inválido')
            status.add(msg,'erro')
            return
        dados = form.keys()
        try:
            dados.pop(dados.index('form.buttons.salvar'))
            dados.pop(dados.index('id'))
            
            #Verifica se tem ligacao para este estado
            ligacoes = session.query(Ligacao).filter_by(estado_id=estado_id)
            if ligacoes:
                #Deleta as ligacoes
                session.query(Ligacao).filter_by(estado_id=estado_id).delete()
                session.flush()

            #Insere ligacoes
            for dado in dados:
                permissao, papel = dado.split('|')
                session.add(Ligacao(estado_id, papel, permissao))
                session.flush()
        except IntegrityError, e:
            msg = _(u'Falha de integridade relacional: ' + str(e))
            status.add(msg, 'error')
            raise
        else:
            msg = _(u'Salvo com sucesso')
            status.add(msg,'info')
            url = '%s?id=%s' % (self.__name__,estado_id)
            go(url)
            return True
