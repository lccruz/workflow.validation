# -*- coding: utf-8 -*-

from five import grok
from plone.app.layout.navigation.interfaces import INavigationRoot

from workflow.validation.browser.forms import base
from workflow.validation.config import MessageFactory as _
from workflow.validation.config import session
from workflow.validation.interfaces import IPapel
from workflow.validation.db import Papel

class PapelAddForm(base.AddFormWv):
    """
        Formulário de cadastro de um Papel.
    """

    grok.context(INavigationRoot)
    grok.name('add-papel')
    grok.require('cmf.ManagePortal')
    
    schema = IPapel
    klass = Papel
    label = _(u'Adicionar Papel')
    description = _(u'Formulário de cadastro de um Papel')

    def createAndAdd(self, data):
        p = Papel(data['titulo'])
        session.add(p)
        session.flush()


class PapelEditForm(base.EditFormWv):
    """
        Formulário de edição de um Papel.
    """

    grok.context(INavigationRoot)
    grok.name('edit-papel')
    grok.require('cmf.ManagePortal')

    schema = IPapel
    klass = Papel
    label = _(u'Editar Papel')
    descrition = _(u'Formulário de edição de um Papel.')


class PapelShowForm(base.ShowFormWv):
    """
        Formulário de visualização de um Papel.
    """
    
    grok.context(INavigationRoot)
    grok.name('show-papel')
    grok.require('cmf.ManagePortal')

    schema = IPapel
    klass = Papel
    label = _(u'Detalhes do Papel')
    description = _(u'Formulário de visualização de uma Papel.')
