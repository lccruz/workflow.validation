# -*- coding: utf-8 -*-

from five import grok
from plone.app.layout.navigation.interfaces import INavigationRoot

from workflow.validation.browser.forms import base
from workflow.validation.config import MessageFactory as _
from workflow.validation.config import session
from workflow.validation.interfaces import IPermissao
from workflow.validation.db import Permissao

class PermissaoAddForm(base.AddFormWv):
    """
        Formulário de cadastro de um Permissão.
    """

    grok.context(INavigationRoot)
    grok.name('add-permissao')
    grok.require('cmf.ManagePortal')
    
    schema = IPermissao
    klass = Permissao
    label = _(u'Adicionar Permissao')
    description = _(u'Formulário de cadastro de um Permissão')

    def createAndAdd(self, data):
        p = Permissao(data['titulo'], data['titulo_permissao'])
        session.add(p)
        session.flush()


class PermissaoEditForm(base.EditFormWv):
    """
        Formulário de edição de um Permissão.
    """

    grok.context(INavigationRoot)
    grok.name('edit-permissao')
    grok.require('cmf.ManagePortal')

    schema = IPermissao
    klass = Permissao
    label = _(u'Editar Permissão')
    descrition = _(u'Formulário de edição de um Permissão.')


class PermissaoShowForm(base.ShowFormWv):
    """
        Formulário de visualização de um Permissão.
    """
    
    grok.context(INavigationRoot)
    grok.name('show-permissao')
    grok.require('cmf.ManagePortal')

    schema = IPermissao
    klass = Permissao
    label = _(u'Detalhes do Permissão')
    description = _(u'Formulário de visualização de uma Permissão.')
