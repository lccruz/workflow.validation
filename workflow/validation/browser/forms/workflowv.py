# -*- coding: utf-8 -*-

from five import grok
from plone.app.layout.navigation.interfaces import INavigationRoot

from workflow.validation.browser.forms import base
from workflow.validation.config import MessageFactory as _
from workflow.validation.config import session
from workflow.validation.interfaces import IWorkflowv
from workflow.validation.db import Workflowv

class WorkflowvAddForm(base.AddFormWv):
    """
        Formulário de cadastro de um Workflow.
    """

    grok.context(INavigationRoot)
    grok.name('add-workflowv')
    grok.require('cmf.ManagePortal')
    
    schema = IWorkflowv
    klass = Workflowv
    label = _(u'Adicionar Workflow')
    description = _(u'Formulário de cadastro de um Workflow')

    def createAndAdd(self, data):
        w = Workflowv(data['titulo'])
        session.add(w)
        session.flush()


class WorkflowvEditForm(base.EditFormWv):
    """
        Formulário de edição de um Workflow.
    """

    grok.context(INavigationRoot)
    grok.name('edit-workflowv')
    grok.require('cmf.ManagePortal')

    schema = IWorkflowv
    klass = Workflowv
    label = _(u'Editar Workflow')
    descrition = _(u'Formulário de edição de um Workflow.')


class WorkflowvShowForm(base.ShowFormWv):
    """
        Formulário de visualização de um Workflow.
    """
    
    grok.context(INavigationRoot)
    grok.name('show-workflowv')
    grok.require('cmf.ManagePortal')

    schema = IWorkflowv
    klass = Workflowv
    label = _(u'Detalhes do Workflow')
    description = _(u'Formulário de visualização de uma Workflow.')
