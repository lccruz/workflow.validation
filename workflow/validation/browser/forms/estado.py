# -*- coding: utf-8 -*-

from five import grok
from plone.app.layout.navigation.interfaces import INavigationRoot

from workflow.validation.browser.forms import base
from workflow.validation.config import MessageFactory as _
from workflow.validation.config import session
from workflow.validation.interfaces import IEstado
from workflow.validation.db import Estado

class EstadoAddForm(base.AddFormWv):
    """
        Formulário de cadastro de um Estado.
    """

    grok.context(INavigationRoot)
    grok.name('add-estado')
    grok.require('cmf.ManagePortal')
    
    schema = IEstado
    klass = Estado
    label = _(u'Adicionar Estado')
    description = _(u'Formulário de cadastro de um Estado')

    def createAndAdd(self, data):
        e = Estado(data['workflowv_id'], data['titulo'])
        session.add(e)
        session.flush()


class EstadoEditForm(base.EditFormWv):
    """
        Formulário de edição de um Estado.
    """

    grok.context(INavigationRoot)
    grok.name('edit-estado')
    grok.require('cmf.ManagePortal')

    schema = IEstado
    klass = Estado
    label = _(u'Editar Estado')
    descrition = _(u'Formulário de edição de um Estado.')


class EstadoShowForm(base.ShowFormWv):
    """
        Formulário de visualização de um Estado.
    """
    
    grok.context(INavigationRoot)
    grok.name('show-estado')
    grok.require('cmf.ManagePortal')

    schema = IEstado
    klass = Estado
    label = _(u'Detalhes do Estado')
    description = _(u'Formulário de visualização de uma Estado.')
