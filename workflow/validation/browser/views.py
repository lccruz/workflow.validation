# -*- coding: utf-8 -*-

import os
import shutil
import zipfile
import tempfile

from xml.dom.minidom import Document
from five import grok
from plone.app.layout.navigation.interfaces import INavigationRoot

from workflow.validation.config import session
from workflow.validation import db
from workflow.validation.config import MessageFactory as _
from workflow.validation.nav import url
from workflow.validation.browser.content_export import get_arquivo_read, get_arquivo_test

class BaseListView(object):
    
    def show_url(self, id, vs=None):
        vs = self.view_sufix if vs is None else vs
        return url('show-'+vs, id=id)

    def add_url(self, vs=None):
        vs = self.view_sufix if vs is None else vs
        return url('add-'+vs)

    def generic_url(self, paran_url, paran=False):
        if paran:
            return url(paran_url,id=paran)
        return url(paran_url)


class InicialListView(grok.View, BaseListView):

    grok.name('inicial')
    grok.context(INavigationRoot)
    grok.require('cmf.ManagePortal')

    def update(self):
        """
            View inicial .pt
        """
        # desabilita a div #edit-bar
        self.request.set('disable_border', True)
        

class WorkflowvListView(grok.View, BaseListView):

    grok.name('list-workflowv')
    grok.context(INavigationRoot)
    grok.require('cmf.ManagePortal')

    dados = []
    view_sufix = 'workflowv'

    def update(self):
        """
            Computa os dados antes de renderizar o .pt
        """
        # desabilita a div #edit-bar
        self.request.set('disable_border', True)
        self.dados = []
        items = session.query(db.Workflowv).all()
        for item in items:
            self.dados.append({'id': item.id,
                               'titulo': item.titulo})


class EstadoListView(grok.View, BaseListView):

    grok.name('list-estado')
    grok.context(INavigationRoot)
    grok.require('cmf.ManagePortal')

    dados = []
    view_sufix = 'estado'

    def update(self):
        """
            Computa os dados antes de renderizar o .pt
        """
        # desabilita a div #edit-bar
        self.request.set('disable_border', True)
        self.dados = []
        items = session.query(db.Estado).all()
        for item in items:
            self.dados.append({'id': item.id,
                               'workflowv_id': item.workflowv_id,
                               'workflowv_titulo': item.workflowv.titulo,
                               'titulo': item.titulo})


class PapelListView(grok.View, BaseListView):

    grok.name('list-papel')
    grok.context(INavigationRoot)
    grok.require('cmf.ManagePortal')

    dados = []
    view_sufix = 'papel'

    def update(self):
        self.request.set('disable_border', True)
        self.dados = []
        items = session.query(db.Papel).all()
        for item in items:
            self.dados.append({'id': item.id,
                               'titulo': item.titulo})


class PermissaoListView(grok.View, BaseListView):

    grok.name('list-permissao')
    grok.context(INavigationRoot)
    grok.require('cmf.ManagePortal')

    dados = []
    view_sufix = 'permissao'

    def update(self):
        self.request.set('disable_border', True)
        self.dados = []
        items = session.query(db.Permissao).all()
        for item in items:
            self.dados.append({'id': item.id,
                               'titulo': item.titulo})

class ExportWorkflowv(grok.View):
    
    grok.name('export-workflowv')
    grok.context(INavigationRoot)
    grok.require('cmf.ManagePortal')

    def _gera_xml(self, workflow_id):
        doc = Document()
        root = doc.createElement('workflow')
        workflow = session.query(db.Workflowv).get(workflow_id)
        root.setAttribute( "name", workflow.titulo)
        doc.appendChild(root)
        
        for estado in workflow.estado:
            estadoxml = doc.createElement('state')
            estadoxml.setAttribute("name", estado.titulo)
            root.appendChild(estadoxml)
            permissoes = session.query(db.Ligacao.permissao_id).filter_by(estado_id=estado.id).group_by(db.Ligacao.permissao_id).order_by('permissao_id').all()
            for permissao in permissoes:
                papeis = session.query(db.Ligacao).filter(db.Ligacao.estado_id==estado.id,db.Ligacao.permissao_id==permissao[0]).all()
                permissaoxml = doc.createElement('permission')
                permissaoxml.setAttribute("name", papeis[0].permissao.titulo_permissao)
                estadoxml.appendChild(permissaoxml)
                for papel in papeis:
                    text = doc.createTextNode(papel.papel.titulo)
                    rolexml = doc.createElement('role')
                    rolexml.appendChild(text)
                    permissaoxml.appendChild(rolexml)
        return doc.toprettyxml()

    def render(self):
        workflow = session.query(db.Workflowv).get(self.request.get('id'))
        if not workflow:
            return 
        tempdir = tempfile.mkdtemp(dir='/tmp/')

        zipname = "%s/%s.zip" % (tempdir,'zipfile')
        zf = zipfile.ZipFile(zipname, mode='w')
        
        self.context.REQUEST.RESPONSE.setHeader('Content-Type','application/zip')
        self.context.REQUEST.RESPONSE.setHeader('Content-Disposition','attachment; filename=%s'%(zipname))

        try:
            #zf.write(outfile.name)
            zf.writestr("data.xml", self._gera_xml(self.request.get('id')))
            zf.writestr("read.py", get_arquivo_read())
            zf.writestr("test.py", get_arquivo_test())
        finally:
            zf.close()

        fp = open(zipname, 'rb')
        self.context.REQUEST.RESPONSE.write(fp.read())
        fp.close()

        shutil.rmtree(tempdir)
        return
