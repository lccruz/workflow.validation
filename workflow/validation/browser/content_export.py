#-*- coding: utf-8 -*-

def get_arquivo_read():
    """ Gera arquivo com DEF que le o XML
    """
    arquivo_read ="""# -*- coding:utf-8 -*-

import os
from xml.dom import minidom

def get_workflow_xml(workflow_file, padrao=True):

    if padrao:
        tags = dict(workflow = {'tag':'dc-workflow', 'attr':'name'},
                    state = {'tag':'state', 'attr':'state_id'},
                    permission = {'tag':'permission-map', 'attr':'name'},
                    roles = {'tag':'permission-role', 'attr':''})
    else:
        tags = dict(workflow = {'tag':'workflow', 'attr':'name'},
                    state = {'tag':'state', 'attr':'name'},
                    permission = {'tag':'permission', 'attr':'name'},
                    roles = {'tag':'role', 'attr':'name'})

    PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))
    try:
        doc = minidom.parse(("%s/%s") % (PROJECT_PATH, workflow_file))
    except:
        return False
    workflow_name = doc.getElementsByTagName(tags['workflow']['tag'])[0].getAttribute(tags['workflow']['attr'])
    permission_roles = {}
    states_permission_roles = {}
    role_list = []

    for state in doc.getElementsByTagName(tags['state']['tag']):
        for permission in state.getElementsByTagName(tags['permission']['tag']):
            for role in permission.getElementsByTagName(tags['roles']['tag']):
                role_list.append(role.childNodes[0].data)
            permission_roles[permission.getAttribute(tags['permission']['attr'])] = tuple(role_list)
            role_list = []
        states_permission_roles[state.getAttribute(tags['state']['attr'])] = permission_roles
        permission_roles = {}
    return dict(workflow_name=workflow_name, states_permission_roles=states_permission_roles)"""
    return arquivo_read


def get_arquivo_test():
    """ Gera arquivo de teste
    """

    arquivo_test = """#-*- coding: utf-8 -*-
import unittest
from read import *

class TestWorkflow(unittest.TestCase):
    
    def setUp(self):
        #definicoes iniciais
        self.requisites = get_workflow_xml('data.xml', False)
        self.requisites_name = self.requisites.get('workflow_name')
        self.requisites_content = self.requisites.get('states_permission_roles')
        self.requisites_content_states = self.requisites_content.keys()
        self.requisites_content_permissions = self.requisites_content.values()[0].keys()
        #workflow Plone
        self.definition = get_workflow_xml('definition.xml', True)
        self.definition_name = self.definition.get('workflow_name')
        self.definition_content = self.definition.get('states_permission_roles')
        self.definition_content_states = self.definition_content.keys()
        self.definition_content_permissions = self.definition_content.values()[0].keys()

    def test_states(self):
        self.assertItemsEqual(self.requisites_content_states, self.definition_content_states)

    def test_permissions(self):
        self.assertItemsEqual(self.requisites_content_permissions, self.definition_content_permissions)

    def test_permissions_roles(self):
        for state in self.requisites_content_states:
            permissions = self.requisites_content.get(state)
            for permission,roles in permissions.items():
                self.assertItemsEqual(roles,self.definition_content[state][permission])
 
if __name__ == '__main__':
    unittest.main()"""
    return arquivo_test
