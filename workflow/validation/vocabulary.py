# -*- coding: utf-8 -*-

from zope.interface import implements
from zope.component import provideUtility
from zope.app.component.hooks import getSite
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
from zope.schema.interfaces import IVocabularyFactory

from workflow.validation.config import MessageFactory as _
from workflow.validation.config import session
from workflow.validation import db
               
class WvVocabulary(object):

    implements(IVocabularyFactory)

    def __call__(self, context):
        query = session.query(db.Workflowv).order_by(db.Workflowv.titulo).all()
        return SimpleVocabulary([SimpleTerm(dado.id, dado.id, dado.titulo) for dado in query])

WvVocabularyFactory = WvVocabulary()
provideUtility(WvVocabularyFactory, IVocabularyFactory,
               name='workflow.validation.wv-vocab')


class PermissionVocabulary(object):

    implements(IVocabularyFactory)

    def __call__(self, context):
        site = getSite()
        return SimpleVocabulary([SimpleTerm(dado, dado, dado) for dado in site.possible_permissions()])

PermissionVocabularyFactory = PermissionVocabulary()
provideUtility(PermissionVocabularyFactory, IVocabularyFactory,
               name='workflow.validation.permission-vocab')
