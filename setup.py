from setuptools import setup, find_packages
import os

version = '1.0'

setup(name='workflow.validation',
      version=version,
      description="",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Framework :: Plone",
        "Programming Language :: Python",
        ],
      keywords='Workflow Plone Validation Tests',
      author='Luciano Camargo Cruz',
      author_email='luciano@lccruz.net',
      url='https://bitbucket.org/lccruz/workflow.validation',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['workflow'],
      include_package_data=True,
      zip_safe=False,
      install_requires=['setuptools',
                        'z3c.saconfig',
                        'psycopg2',
                        'five.grok',
                        'plone.directives.form',
      ],
      entry_points="""
      # -*- Entry points: -*-

      [z3c.autoinclude.plugin]
      target = plone
      """,
#      setup_requires=["PasteScript"],
#      paster_plugins=["ZopeSkel"],
      )
